package net.gryfdev.whatislove;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Toast;

public class WhatIsLove extends Activity implements SensorEventListener {
    private SensorManager sensorManager;
    private MediaPlayer whatIsLove;

    private long lastUpdate = -1;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 925;

    boolean foreground = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_is_love);

        Toast.makeText(getApplicationContext(), "Shake it...", Toast.LENGTH_SHORT).show();

        sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        whatIsLove = MediaPlayer.create(this, R.raw.love);
    }

    @Override
    protected void onPause() {
        foreground = false;
        sensorManager.unregisterListener(this);

        super.onPause();
    }
    @Override
    protected void onResume() {
        super.onResume();
        foreground = true;

        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_GAME);
    }

    public void onAccuracyChanged(Sensor sensor,int accuracy){

    }

    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER && foreground) {
            long curTime = System.currentTimeMillis();
            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float x=event.values[0];
                float y=event.values[1];
                float z=event.values[2];

                float speed = Math.abs(x+y+z - last_x - last_y - last_z) / diffTime * 10000;

                if (speed > SHAKE_THRESHOLD) {
                    if (!whatIsLove.isPlaying()) {
                        whatIsLove.start();
                    }
                } else {
                    if (whatIsLove.isPlaying()) {
                        whatIsLove.stop();
                        whatIsLove = MediaPlayer.create(this, R.raw.love);
                    }
                }

                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }
}
